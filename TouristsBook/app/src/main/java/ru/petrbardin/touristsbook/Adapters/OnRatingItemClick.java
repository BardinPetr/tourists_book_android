package ru.petrbardin.touristsbook.Adapters;

import android.view.View;

import java.util.ArrayList;

import ru.petrbardin.touristsbook.DataProvider.API;

/**
 * Created by комп on 18.10.2017.
 */

public interface OnRatingItemClick {
    void onClick(View v, API.User u);
}
