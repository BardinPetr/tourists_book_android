package ru.petrbardin.touristsbook.Dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import ru.petrbardin.touristsbook.R;

/**
 * Created by комп on 24.09.2017.
 */

public class ResultDialog extends Dialog implements android.view.View.OnClickListener {
    public Activity c;
    public Dialog d;
    public Button yes, no;
    public int st;

    public ResultDialog(Activity a, int st) {
        super(a);
        this.c = a;
        this.st = st;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        if(st == -10){
            setContentView(R.layout.dialog_exit);
            yes = (Button) findViewById(R.id.btn_yes);
            yes.setOnClickListener(this);
            no = (Button) findViewById(R.id.btn_no);
            no.setOnClickListener(this);
        } else if(st == 0){
            setContentView(R.layout.dialog_captured);
            yes = (Button) findViewById(R.id.btn_yes);
            yes.setOnClickListener(this);
        } else if(st == -1){
            setContentView(R.layout.dialog_toofar);
            yes = (Button) findViewById(R.id.btn_yes);
            yes.setOnClickListener(this);
        } else if(st == -2){
            setContentView(R.layout.dialog_alreadycap);
            yes = (Button) findViewById(R.id.btn_yes);
            yes.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_yes:
                dismiss();
                break;
            case R.id.btn_no:
                //c.start_main();
                break;
            default:
                break;
        }
        dismiss();
    }
}