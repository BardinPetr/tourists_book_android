package ru.petrbardin.touristsbook.Fragments;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import ru.petrbardin.touristsbook.Adapters.OnPlaceItemClick;
import ru.petrbardin.touristsbook.Adapters.ProfileItemAdapter;
import ru.petrbardin.touristsbook.Adapters.ProfileItemAdapterAch;
import ru.petrbardin.touristsbook.DataProvider.API;
import ru.petrbardin.touristsbook.R;


public class ProfileAchievementsPageFragment extends Fragment implements OnPlaceItemClick {
    private ArrayList<API.Achievement> data;
    private ArrayList<API.Achievement> data_a;
    private RecyclerView recyclerView;
    private API.User user;

    public ProfileAchievementsPageFragment() {
    }

    public static ProfileAchievementsPageFragment newInstance(String param1, String param2) {
        ProfileAchievementsPageFragment fragment = new ProfileAchievementsPageFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile_achievements_page, container, false);

        recyclerView = (RecyclerView) v.findViewById(R.id.pp_a_recycler_view);

        ProfileItemAdapterAch adapter = new ProfileItemAdapterAch(this.data, this.user, this, this.getContext());

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this.getContext(), 3);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new ProfileCapturedPageFragment.GridSpacingItemDecoration(3, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void setData(ArrayList<API.Achievement> achiev, API.User u){
        user = u;
        data = achiev;
    }

    @Override
    public void onClick(View v, int uid) {

    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
