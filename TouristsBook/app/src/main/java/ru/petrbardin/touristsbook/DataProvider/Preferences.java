package ru.petrbardin.touristsbook.DataProvider;
/**
 * Created by BardinPetr on 03.09.2017.
 */


import android.content.Context;
import android.content.SharedPreferences;
//import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;

public class Preferences extends AppCompatActivity {
    final String PREF_LOGIN = "is_login";
    final String PREF_USER = "username";
    final String PREF_PASS = "password";
    final String PREF_EMAIL = "email";

    public SharedPreferences sPref;
    public SharedPreferences.Editor ed;
    public Preferences(Context ctx){
        sPref = ctx.getSharedPreferences("LOGIN", MODE_PRIVATE);
        ed = sPref.edit();
    }

    public void set(String id, String e ){
        //SharedPreferences.Editor ed = sPref.edit();
        ed.putString(id, e);
        ed.apply();
    }
    public void set(String id, Integer e ){
        //SharedPreferences.Editor ed = sPref.edit();
        ed.putInt(id, e);
        ed.apply();
    }
    public void set(String id, Boolean e ){
        //SharedPreferences.Editor ed = sPref.edit();
        ed.putBoolean(id, e);
        ed.apply();
    }

    public Boolean isLoggedIn(){
        return sPref.getBoolean(PREF_LOGIN, false);
    }

    public void LogIn(){
        set(PREF_LOGIN, true);
    }
    public void LogOut() { set(PREF_LOGIN, false); }

    public String Username(){
        return sPref.getString(PREF_USER, "");
    }
    public String Password(){
        return sPref.getString(PREF_PASS, "");
    }
    public String Email(){
        return sPref.getString(PREF_EMAIL, "");
    }

    public void Username(String a) {
        set(PREF_USER, a);
    }
    public void Password(String a) {
        set(PREF_PASS, a);
    }
    public void Email(String a) {
        set(PREF_EMAIL, a);
    }
}
