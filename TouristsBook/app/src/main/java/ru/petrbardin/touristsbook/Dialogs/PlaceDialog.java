package ru.petrbardin.touristsbook.Dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import ru.petrbardin.touristsbook.DataProvider.API;
import ru.petrbardin.touristsbook.MainActivity;
import ru.petrbardin.touristsbook.R;

public class PlaceDialog extends Dialog implements android.view.View.OnClickListener {
    public Activity c;
    public Dialog d;
    public CardView cv;
    public TextView tv;
    public ImageView iv;
    public int st;
    public API.Place p;

    public PlaceDialog(Activity a, API.Place p) {
        super(a);
        this.c = a;
        this.p = p;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_placeinfo);

        tv = (TextView) findViewById(R.id.pi_d_tv);
        iv = (ImageView) findViewById(R.id.pi_d_iv);
        cv = (CardView) findViewById(R.id.pi_d_cv);

        tv.setText(p.getName());
        Glide.with(getContext())
                .load(p.photo)
                .into(iv);

        cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.openPlaceInfo(getContext(), p);
                dismiss();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_yes:
                dismiss();
                break;
            case R.id.btn_no:
                //c.start_main();
                break;
            default:
                break;
        }
        dismiss();
    }
}