package ru.petrbardin.touristsbook.Adapters;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import ru.petrbardin.touristsbook.DataProvider.API;
import ru.petrbardin.touristsbook.R;

public class ProfileItemAdapterAch extends RecyclerView.Adapter<ProfileItemAdapterAch.ItemViewHolder> {
    API.User user;
    List<API.Achievement> items;
    OnPlaceItemClick listener;
    Context ctx;

    public ProfileItemAdapterAch(List<API.Achievement> items, API.User u, OnPlaceItemClick l, Context ctx) {
        this.user = u;
        this.items = items;
        this.listener = l;
        this.ctx = ctx;
    }

    @Override
    public ProfileItemAdapterAch.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.pa_rv_item_1, parent, false);
        return new ProfileItemAdapterAch.ItemViewHolder(v, this.ctx);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(ProfileItemAdapterAch.ItemViewHolder holder, int position) {
        holder.bind(items.get(position), this.listener);

        if(this.user.ach.contains(items.get(position).uid))
            Glide.with(this.ctx).load(items.get(position).icon).into(holder.iv);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView tv;
        ImageView iv;
        CardView cv;
        Context ctx;

        public ItemViewHolder(View itemView, Context ctx) {
            super(itemView);

            this.ctx = ctx;

            tv = (TextView) itemView.findViewById(R.id.pa1_item_tv);
            iv = (ImageView) itemView.findViewById(R.id.pa1_item_iv);
            cv = (CardView) itemView.findViewById(R.id.pa1_item_cv);
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        void bind(final API.Achievement a, final OnPlaceItemClick listener) {
            tv.setText(a.name);
            cv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClick(view, a.uid);
                }
            });
        }
    }
}
