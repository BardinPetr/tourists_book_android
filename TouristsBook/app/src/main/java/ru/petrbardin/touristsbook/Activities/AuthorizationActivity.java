package ru.petrbardin.touristsbook.Activities;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.Nullable;
//import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.koushikdutta.async.future.FutureCallback;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import ru.petrbardin.touristsbook.DataProvider.API;
import ru.petrbardin.touristsbook.MainActivity;
import ru.petrbardin.touristsbook.DataProvider.Preferences;
import ru.petrbardin.touristsbook.R;

public class AuthorizationActivity extends AppCompatActivity {
    String url = "192.168.0.65";
    //String url = "192.168.43.98";

    Button btn_login;
    Button btn_reg;
    Button btn_openReg;
    EditText user_login;
    EditText user_email;
    EditText user_pass;
    TextView user_email_tv;
    ProgressBar pb;

    Intent main_intent;
    API api;
    public Preferences pref;

    Boolean a = false;
    private Activity This;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        This = this;

        main_intent = new Intent(This, MainActivity.class);
        main_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

        api = new API(getBaseContext());
        pref = new Preferences(This);

        if(getIntent().getIntExtra("ACTION", 0) == 0) {
            setContentView(R.layout.splash);

            Thread welcomeThread = new Thread() {
                @Override
                public void run() {
                    try {
                        super.run();
                        sleep(8000);
                    } catch (Exception ignored) {
                    } finally {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (pref.isLoggedIn()) {
                                    selectstrategy_activity();
                                } else {
                                    pref.LogOut();
                                    initLayout();
                                }
                            }
                        });
                    }
                }
            };
            Glide.with(this)
                    .load(R.drawable.splash)
                    .into((ImageView) findViewById(R.id.imageView3));
            welcomeThread.start();
        }
        else{
            initLayout();
            pref.LogOut();
        }
    }

    public void selectstrategy_activity(){
        setContentView(R.layout.activity_select_strategy);

        View.OnClickListener ss_ib_ocl = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                main_intent.putExtra("LOGIN_TYPE", 2);
                main_intent.putExtra("USER_STRATEGY", v.getId());
                startActivity(main_intent);
                This.finish();
            }
        };

        findViewById(R.id.imageButton).setOnClickListener(ss_ib_ocl);
        findViewById(R.id.imageButton2).setOnClickListener(ss_ib_ocl);
    }

    public void initLayout(){
        setContentView(R.layout.activity_authorization_reg);
        /*
        LayoutInflater inflator = getLayoutInflater();
        View view = inflator.inflate(R.layout.activity_authorization_reg, null, false);
        view.startAnimation(AnimationUtils.loadAnimation(This, android.R.anim.fade_in));
        setContentView(view);
        */

        btn_openReg = (Button) findViewById(R.id.open_reg_btn);
        btn_login = (Button) findViewById(R.id.login_btn);

        user_login = (EditText) findViewById(R.id.user);
        user_pass = (EditText) findViewById(R.id.pass);
        user_email = (EditText) findViewById(R.id.email);
        user_email_tv = (TextView) findViewById(R.id.email_tv);

        pb = (ProgressBar) findViewById(R.id.progressBar);
        pb.setVisibility(View.INVISIBLE);
    }

    public void btn_openReg_onClick(View v) {
        user_email.setVisibility(View.VISIBLE);
        user_email_tv.setVisibility(View.VISIBLE);
        btn_openReg.setVisibility(View.GONE);
        btn_login.setText(R.string.action_register);
        a = true;
    }

    public void btn_login_onClick(final View v) {
        pb.setVisibility(View.VISIBLE);
        if (a) {
            api.register(api.genCred(user_login.getText().toString(), md5(user_pass.getText().toString()), "", "", user_email.getText().toString()), new FutureCallback<API.Result>() {
                @Override
                public void onCompleted(Exception e, @Nullable API.Result result) {
                    if (result == null) {
                        toast("SERVER ERROR");
                    } else {
                        if (!result.status) {
                            toast("USER ALREADY REGISTRED");
                        } else {
                            start_main();
                            a = false;
                            btn_login_onClick(v);
                        }
                    }
                    pb.setVisibility(View.INVISIBLE);
                }
            });
        } else {
            api.login(api.genCred(user_login.getText().toString(), md5(user_pass.getText().toString())), new FutureCallback<API.Result>() {
                @Override
                public void onCompleted(Exception e, API.Result result) {
                    if (result == null) {
                        toast("SERVER ERROR");
                    } else if (result.status == false) {
                        toast("REGISTRATION ERROR");
                    } else {
                        write();
                        start_main();
                    }
                    pb.setVisibility(View.INVISIBLE);
                }
            });
        }
    }

    public void write(){
        pref.LogIn();
        pref.Username(user_login.getText().toString());
        pref.Password(md5(user_pass.getText().toString()));
        pref.Email(user_email.getText().toString());
    }

    public void start_main() {
        if(a){
            main_intent.putExtra("LOGIN_TYPE", 1);
            main_intent.putExtra("LOGIN_USER", user_login.getText().toString());
            main_intent.putExtra("LOGIN_EMAIL", user_email.getText().toString());
            main_intent.putExtra("LOGIN_PASS", md5(user_pass.getText().toString()));
        } else {
            main_intent.putExtra("LOGIN_TYPE", 0);
            main_intent.putExtra("LOGIN_USER", user_login.getText().toString());
            main_intent.putExtra("LOGIN_PASS", md5(user_pass.getText().toString()));
        }
        selectstrategy_activity();
    }

    public void toast(String a){
        Toast.makeText(getApplicationContext(), a, Toast.LENGTH_SHORT).show();
    }

    public String md5(String s) {
        try {
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest)
                hexString.append(Integer.toHexString(0xFF & aMessageDigest));
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
}
