package ru.petrbardin.touristsbook.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.koushikdutta.async.future.FutureCallback;

import java.util.ArrayList;
import java.util.List;

import ru.petrbardin.touristsbook.Adapters.OnPlaceItemClick;
import ru.petrbardin.touristsbook.Adapters.OnRatingItemClick;
import ru.petrbardin.touristsbook.Adapters.RatingItemAdapter;
import ru.petrbardin.touristsbook.DataProvider.API;
import ru.petrbardin.touristsbook.DataProvider.Preferences;
import ru.petrbardin.touristsbook.MainActivity;
import ru.petrbardin.touristsbook.R;

public class RatingFragment extends Fragment implements OnRatingItemClick {
    private API api;
    private Preferences pref;

    ArrayList<API.User> rating;
    private String t_Username;
    private String t_Password;
    private RecyclerView rv;

    public RatingFragment() {}

    public static RatingFragment newInstance() {
        RatingFragment fragment = new RatingFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_rating, container, false);
        api = new API(getContext());
        pref = new Preferences(getContext());

        api.getRating(api.genCred(pref.Username(), pref.Password(), new double[]{54.271802, 48.300644}), new FutureCallback<API._Rating>() {
            @Override
            public void onCompleted(Exception e, API._Rating r_res) {
                if(e == null && r_res != null) {
                    rating = generateRating(r_res.data);
                    createRV(v);
                }
            }
        });
        return v;
    }

    public void createRV(View v){
        RatingItemAdapter adapter = new RatingItemAdapter(rating, this, this.getContext());
        rv = (RecyclerView) v.findViewById(R.id.rf_recyclerview);
        rv.setLayoutManager(new LinearLayoutManager(this.getContext()));
        rv.setHasFixedSize(true);
        rv.setAdapter(adapter);
    }

    public void setUserData(String a, String b){
        t_Username = a;
        t_Password = b;
    }

    public ArrayList<API.User> generateRating(API.Rating[] data){
        final ArrayList<API.User> a = new ArrayList<>();

        for(API.Rating r: data){
            API.User u = api.getUser_s(api.genCred(t_Username, t_Password, new double[]{54.271802, 48.300644}), r.uid);
            if (u != null)
                a.add(u);
        }
        return a;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v, API.User u) {
        MainActivity.openProfile(getContext(), u);
    }
}
