package ru.petrbardin.touristsbook.Activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.koushikdutta.async.future.FutureCallback;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;

import ru.petrbardin.touristsbook.DataProvider.API;
import ru.petrbardin.touristsbook.DataProvider.Preferences;
import ru.petrbardin.touristsbook.Fragments.ProfileAchievementsPageFragment;
import ru.petrbardin.touristsbook.Fragments.ProfileCapturedPageFragment;
import ru.petrbardin.touristsbook.Fragments.ProfileSettingsPageFragment;
import ru.petrbardin.touristsbook.R;

public class ProfileActivity extends AppCompatActivity {
    TextView user_n;
    TextView user_e;
    TextView user_s;
    TextView user_r;
    TextView user_a;
    TextView user_c;
    ImageView user_p;

    Preferences pref;
    API api;

    API.User user;
    ArrayList<API.Place> places = new ArrayList<API.Place>();
    ArrayList<API.Achievement> achiev = new ArrayList<API.Achievement>();

    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private ProfileAchievementsPageFragment ppf_ach;
    private ProfileCapturedPageFragment ppf_cap;
    private ProfileSettingsPageFragment ppf_set;

    private Activity This;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        This = this;

        pref = new Preferences(this.getApplicationContext());
        api = new API(this.getApplicationContext());

        user = (API.User) getIntent().getParcelableExtra("userdata");
        places = getIntent().getParcelableArrayListExtra("placesdata");

        ppf_ach = new ProfileAchievementsPageFragment();
        ppf_cap = new ProfileCapturedPageFragment();
        ppf_set = new ProfileSettingsPageFragment();

        ppf_cap.setData(places, api.getCapturedPlaces(places, user));
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.p_frgmCont, ppf_cap);
        fragmentTransaction.commit();

        Toolbar toolbar = (Toolbar) findViewById(R.id.p_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    This.finish();
                }
            }
        );

        initHeader();

        api.getAch(api.genCred(pref.Username(), pref.Password(), new double[]{54.271802, 48.300644}), new FutureCallback<API._Achievements>() {
            @Override
            public void onCompleted(Exception e, API._Achievements res) {
                if(e == null && res != null){
                    achiev = new ArrayList<API.Achievement>();
                    Collections.addAll(achiev, res.data);
                }
            }
        });

        TabLayout tl = (TabLayout) findViewById(R.id.p_tabs);
        tl.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int id = tab.getPosition();
                fragmentTransaction = fragmentManager.beginTransaction();
                switch(id){
                    case 0:
                        ppf_cap.setData(places, api.getCapturedPlaces(places, user));
                        fragmentTransaction.replace(R.id.p_frgmCont, ppf_cap);
                        break;
                    case 1:
                        ppf_ach.setData(achiev, user);
                        fragmentTransaction.replace(R.id.p_frgmCont, ppf_ach);
                        break;
                    case 2:
                        fragmentTransaction.replace(R.id.p_frgmCont, ppf_set);
                        break;
                }

                fragmentTransaction.commit();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {}
            @Override
            public void onTabReselected(TabLayout.Tab tab) {}
        });
    }

    private void initHeader(){
        user_n = (TextView) findViewById(R.id.p_ndh_username);
        user_e = (TextView) findViewById(R.id.p_ndh_email);
        user_s = (TextView) findViewById(R.id.p_ndh_score);
        user_r = (TextView) findViewById(R.id.p_ndh_ach);
        user_a = (TextView) findViewById(R.id.p_ndh_cap);
        user_c = (TextView) findViewById(R.id.p_ndh_rating);
        user_p = (ImageView) findViewById(R.id.p_imageView4);

        user_n.setText(user.user);
        user_e.setText(user.email);

        user_s.setText(String.valueOf(user.score));
        user_r.setText(String.valueOf(user.ach.size()));
        user_a.setText(String.valueOf(user.ach.size()));
        user_c.setText(String.valueOf(user.captured.size()));

        Glide.with(getApplicationContext()).load(user.photo).into(user_p);
    }

}
