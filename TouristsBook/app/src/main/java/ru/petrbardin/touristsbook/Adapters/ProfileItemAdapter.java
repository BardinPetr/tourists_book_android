package ru.petrbardin.touristsbook.Adapters;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import ru.petrbardin.touristsbook.DataProvider.API;
import ru.petrbardin.touristsbook.R;

public class ProfileItemAdapter extends RecyclerView.Adapter<ProfileItemAdapter.ItemViewHolder> {
    Context ctx;
    List<API.Place> items;
    OnPlaceItemClick listener;

    public ProfileItemAdapter(List<API.Place> items, OnPlaceItemClick l, Context ctx) {
        this.items = items;
        this.listener = l;
        this.ctx = ctx;
    }

    @Override
    public ProfileItemAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.pa_rv_item_0, parent, false);
        return new ProfileItemAdapter.ItemViewHolder(v);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(ProfileItemAdapter.ItemViewHolder holder, int position) {
        holder.bind(items.get(position), this.listener);
        Glide.with(this.ctx).load(items.get(position).photo).into(holder.iv);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView tv;
        ImageView iv;
        CardView cv;

        public ItemViewHolder(View itemView) {
            super(itemView);

            tv = (TextView) itemView.findViewById(R.id.pa0_item_tv);
            iv = (ImageView) itemView.findViewById(R.id.pa0_item_iv);
            cv = (CardView) itemView.findViewById(R.id.pa0_item_cv);
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        void bind(final API.Place a, final OnPlaceItemClick listener) {
            tv.setText(a.getName());
            cv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClick(view, a.uid);
                }
            });
        }
    }
}
