package ru.petrbardin.touristsbook.DataProvider;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.android.internal.util.Predicate;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import ru.petrbardin.touristsbook.R;

public class API {
    /*
     *  Data classes
     */
    public static class Result {
        public Boolean status;
    }

    public static class Place implements Parcelable {
        public Integer uid;
        public String name;
        public Integer reward;
        public double[] gps = {-1.1, -1.1};
        public String description;
        public String photo;
        public String belongs;
        public String url;


        public Integer getUid() {
            return uid;
        }

        public void setUid(Integer uid) {
            this.uid = uid;
        }

        public Integer getReward() {
            return reward;
        }

        public void setReward(Integer reward) {
            this.reward = reward;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(this.uid);
            dest.writeString(this.name);
            dest.writeValue(this.reward);
            dest.writeDoubleArray(this.gps);
            dest.writeString(this.description);
            dest.writeString(this.photo);
            dest.writeString(this.belongs);
            dest.writeString(this.url);
        }

        public Place() {
        }

        protected Place(Parcel in) {
            this.uid = (Integer) in.readValue(Integer.class.getClassLoader());
            this.name = in.readString();
            this.reward = (Integer) in.readValue(Integer.class.getClassLoader());
            this.gps = in.createDoubleArray();
            this.description = in.readString();
            this.photo = in.readString();
            this.belongs = in.readString();
            this.url = in.readString();
        }

        public static final Parcelable.Creator<Place> CREATOR = new Parcelable.Creator<Place>() {
            @Override
            public Place createFromParcel(Parcel source) {
                return new Place(source);
            }

            @Override
            public Place[] newArray(int size) {
                return new Place[size];
            }
        };
    }

    public static class User implements Parcelable {
        public Integer uid;
        public String user;
        public String pass;
        public String photo;
        public String name;
        public String fname;
        public String email;
        public double[] gps = {-1.1, -1.1};
        public Integer team;
        public Integer score;
        public Integer tscore;
        public ArrayList<Integer> captured;
        public ArrayList<Integer> ach;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(this.uid);
            dest.writeString(this.user);
            dest.writeString(this.pass);
            dest.writeString(this.photo);
            dest.writeString(this.name);
            dest.writeString(this.fname);
            dest.writeString(this.email);
            dest.writeDoubleArray(this.gps);
            dest.writeValue(this.team);
            dest.writeValue(this.score);
            dest.writeValue(this.tscore);
            dest.writeList(this.captured);
            dest.writeList(this.ach);
        }

        public User() {
        }

        protected User(Parcel in) {
            this.uid = (Integer) in.readValue(Integer.class.getClassLoader());
            this.user = in.readString();
            this.pass = in.readString();
            this.photo = in.readString();
            this.name = in.readString();
            this.fname = in.readString();
            this.email = in.readString();
            this.gps = in.createDoubleArray();
            this.team = (Integer) in.readValue(Integer.class.getClassLoader());
            this.score = (Integer) in.readValue(Integer.class.getClassLoader());
            this.tscore = (Integer) in.readValue(Integer.class.getClassLoader());
            this.captured = new ArrayList<Integer>();
            in.readList(this.captured, Integer.class.getClassLoader());
            this.ach = new ArrayList<Integer>();
            in.readList(this.ach, Integer.class.getClassLoader());
        }

        public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
            @Override
            public User createFromParcel(Parcel source) {
                return new User(source);
            }

            @Override
            public User[] newArray(int size) {
                return new User[size];
            }
        };
    }

    public static class Achievement implements Parcelable {
        public Integer uid;
        public String name;
        public String desc;
        public String icon;
        public String func;

        public Integer getUid() {
            return uid;
        }

        public String getName() {
            return name;
        }

        public String getDesc() {
            return desc;
        }

        public String getIcon() {
            return icon;
        }

        public String getFunc() {
            return func;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(this.uid);
            dest.writeString(this.name);
            dest.writeString(this.desc);
            dest.writeString(this.icon);
            dest.writeString(this.func);
        }

        public Achievement() {
        }

        protected Achievement(Parcel in) {
            this.uid = (Integer) in.readValue(Integer.class.getClassLoader());
            this.name = in.readString();
            this.desc = in.readString();
            this.icon = in.readString();
            this.func = in.readString();
        }

        public static final Parcelable.Creator<Achievement> CREATOR = new Parcelable.Creator<Achievement>() {
            @Override
            public Achievement createFromParcel(Parcel source) {
                return new Achievement(source);
            }

            @Override
            public Achievement[] newArray(int size) {
                return new Achievement[size];
            }
        };
    }

    public static class Achievements {
        public Integer status;
        public Achievement data;
    }

    public static class _Achievements {
        public Integer status;
        public Achievement[] data;
    }

    public static class Rating implements Parcelable {
        public Integer uid;
        public Integer score;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(this.uid);
            dest.writeValue(this.score);
        }

        public Rating() {
        }

        protected Rating(Parcel in) {
            this.uid = (Integer) in.readValue(Integer.class.getClassLoader());
            this.score = (Integer) in.readValue(Integer.class.getClassLoader());
        }

        public static final Parcelable.Creator<Rating> CREATOR = new Parcelable.Creator<Rating>() {
            @Override
            public Rating createFromParcel(Parcel source) {
                return new Rating(source);
            }

            @Override
            public Rating[] newArray(int size) {
                return new Rating[size];
            }
        };
    }

    public static class _Rating {
        public Integer status;
        public Rating[] data;
    }

    public static class Places {
        public Integer status;
        public ArrayList<Place> data;
    }

    public static class Users {
        public Integer status;
        public User data;
    }

    public static class Capture {
        public Integer status;
        public Integer uid;
        public Integer score;
    }

    public Context context;
    public String url;
    public API(Context c){
        context = c;
        url = c.getString(R.string.prod_server_url);
        url = String.format("http://%s:666", url);
    }

    public ArrayList<Place> getCapturedPlaces(ArrayList<Place> a, User b){
        ArrayList<Place> r = new ArrayList<>();
        for (Place p: a) {
            if (b.captured.contains(p.uid))
                r.add(p);
        }
        return r;
    }

    public ArrayList<Place> getNCapturedPlaces(ArrayList<Place> a, User b){
        ArrayList<Place> r = new ArrayList<>();
        for (Place p: a) {
            if (!b.captured.contains(p.uid))
                r.add(p);
        }
        return r;
    }

    public ArrayList<Achievement> getAchievements(ArrayList<Achievement> a, User b){
        ArrayList<Achievement> r = new ArrayList<>();
        for (Achievement p: a) {
            if (b.ach.contains(p.uid))
                r.add(p);
        }
        return r;
    }

    /*
     *  Capture function
     */

    public void Catch(JsonObject cred, FutureCallback<Capture> cb){
        try{
            Ion.with(context)
                    .load(url + "/capture")
                    .setJsonObjectBody(cred)
                    .as(new TypeToken<Capture>(){})
                    .setCallback(cb);
        }
        catch (Exception e){}
    }

    /*
     *  Basic functions
     */

    public void register(JsonObject cred, FutureCallback<Result> cb){
        try{
            Ion.with(context)
                    .load(url + "/newuser")
                    .setJsonObjectBody(cred)
                    .as(new TypeToken<Result>(){})
                    .setCallback(cb);
        }
        catch (Exception e){}
    }


    public void login(JsonObject cred, FutureCallback<Result> cb){
        try{
            Ion.with(context)
                    .load(url + "/login")
                    .setJsonObjectBody(cred)
                    .as(new TypeToken<Result>(){})
                    .setCallback(cb);
        }
        catch (Exception e){}
    }

    public void getPlaces(JsonObject cred, FutureCallback<Places> cb){
        try{
            Ion.with(context)
                    .load(url + "/getplaces")
                    .setJsonObjectBody(cred)
                    .as(new TypeToken<Places>(){})
                    .setCallback(cb);
        }
        catch (Exception e){}
    }

    public void getPlace(JsonObject cred, int uid, FutureCallback<Places> cb){
        cred.addProperty("uid", uid);
        try{
            Ion.with(context)
                    .load(url + "/getplace")
                    .setJsonObjectBody(cred)
                    .as(new TypeToken<Places>(){})
                    .setCallback(cb);
        }
        catch (Exception e){}
    }

    public void getUser(JsonObject cred, String uid, FutureCallback<Users> cb){
        cred.addProperty("_user", uid);
        try{
            Ion.with(context)
                    .load(url + "/getuser")
                    .setJsonObjectBody(cred)
                    .as(new TypeToken<Users>(){})
                    .setCallback(cb)
                    .wait(3000);
        }
        catch (Exception e){}
    }

    public void getUser(JsonObject cred, int uid, FutureCallback<Users> cb){
        cred.addProperty("_user", uid);
        try{
            Ion.with(context)
                    .load(url + "/getuser")
                    .setJsonObjectBody(cred)
                    .as(new TypeToken<Users>(){})
                    .setCallback(cb);
        }
        catch (Exception e){}
    }

    public User getUser_s(JsonObject cred, int uid){
        cred.addProperty("_user", uid);
        try{
            return Ion.with(context)
                    .load(url + "/getuser")
                    .setJsonObjectBody(cred)
                    .as(new TypeToken<Users>(){})
                    .get().data;
        }
        catch (Exception e){}
        return null;
    }

    public void getRating(JsonObject cred, FutureCallback<_Rating> cb){
        try{
            Ion.with(context)
                    .load(url + "/getrating")
                    .setJsonObjectBody(cred)
                    .as(new TypeToken<_Rating>(){})
                    .setCallback(cb);
        }
        catch (Exception e){}
    }

    public void getAch(JsonObject cred, FutureCallback<_Achievements> cb){
        try{
            Ion.with(context)
                    .load(url + "/getach")
                    .setJsonObjectBody(cred)
                    .as(new TypeToken<_Achievements>(){})
                    .setCallback(cb);
        }
        catch (Exception e){}
    }

    public JsonObject genCred(String login, String pass){
        JsonObject json = new JsonObject();
        json.addProperty("user", login);
        json.addProperty("pass", pass);
        return json;
    }

    public JsonObject genCred(String login, String pass, double[] _gps){
        Gson gson = new Gson();
        JsonParser parser = new JsonParser();
        JsonElement a = parser.parse(gson.toJson(_gps));

        JsonObject json = new JsonObject();
        json.addProperty("user", login);
        json.addProperty("pass", pass);
        json.add("gps", a);
        return json;
    }

    public JsonObject genCred(String login, String pass, String name, String fname, String email){
        JsonObject json = new JsonObject();
        json.addProperty("user", login);
        json.addProperty("pass", pass);
        json.addProperty("name", name);
        json.addProperty("fname", fname);
        json.addProperty("email", email);
        return json;
    }
}



/*
    //EXAMPLES
    API api = new API("http://192.168.0.65:666", getApplicationContext());

    api.Catch(api.genCred("user3", "user3", new double[]{53.203061, 44.995627}), new FutureCallback<API.Capture>() {
        @SuppressLint("SetTextI18n")
        @Override
        public void onCompleted(Exception e, API.Capture res) {
            text.setText(res.status.toString());
        }
    });

    api.getAch(api.genCred("user1", "user1", new double[]{54.271802, 48.300644}), new FutureCallback<API._Achievements>() {
        @SuppressLint("SetTextI18n")
        @Override
        public void onCompleted(Exception e, API._Achievements res) {
            text.setText(res.data[0].name);
        }
    });

    api.getRating(api.genCred("user1", "user1", new double[]{54.271802, 48.300644}), new FutureCallback<API._Rating>() {
        @SuppressLint("SetTextI18n")
        @Override
        public void onCompleted(Exception e, API._Rating res) {
            text.setText(res.data[0].uid.toString());
        }
    });

    api.getUser(api.genCred("user1", "user1", new double[]{54.271802, 48.300644}), "user2", new FutureCallback<API.Users>() {
        @SuppressLint("SetTextI18n")
        @Override
        public void onCompleted(Exception e, API.Users res) {
            text.setText(res.data.email);
        }
    });

    api.getPlace(api.genCred("user1", "user1", new double[]{54.271802, 48.300644}), 1, new FutureCallback<API.Places>() {
        @SuppressLint("SetTextI18n")
        @Override
        public void onComp
        leted(Exception e, API.Places res) {
            text.setText(res.data[0].gps[0].toString());
        }
    });

    api.getPlaces(api.genCred("user1", "user1", new double[]{54.271802, 48.300644}), new FutureCallback<API.Places>() {
        @Override
        public void onCompleted(Exception e, API.Places res) {
            text.setText(res.data[0].gps[0].toString());
        }
    });

    api.login(api.genCred("test", "test"), new FutureCallback<API.Result>() {
        @Override
        public void onCompleted(Exception e, API.Result result) {
            Toast.makeText(getApplicationContext(), result.status.toString(), Toast.LENGTH_LONG).show();
            text.setText(result.status.toString());
        }
    });

    api.register(api.genCred("test", "test", "test", "test", "test"), new FutureCallback<API.Result>() {
        @Override
        public void onCompleted(Exception e, API.Result result) {
            Toast.makeText(getApplicationContext(), result.status.toString(), Toast.LENGTH_LONG).show();
            text.setText(result.status.toString());
        }
    });
*/
