package ru.petrbardin.touristsbook;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
//import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.koushikdutta.async.future.FutureCallback;
import com.mikepenz.iconics.typeface.FontAwesome;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeader;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.Nameable;

import java.util.ArrayList;
import java.util.Timer;

import ru.petrbardin.touristsbook.Activities.PlaceInfoActivity;
import ru.petrbardin.touristsbook.Activities.ProfileActivity;
import ru.petrbardin.touristsbook.DataProvider.API;
import ru.petrbardin.touristsbook.DataProvider.DBContentProviderService;
import ru.petrbardin.touristsbook.DataProvider.Preferences;
import ru.petrbardin.touristsbook.Fragments.MapFragment;
import ru.petrbardin.touristsbook.Fragments.PlacesListFragment;
import ru.petrbardin.touristsbook.Fragments.RatingFragment;


public class MainActivity extends AppCompatActivity {
    public static final String mBroadcastPlacesDataAction = "ru.petrbardin.touristsbook.actions.placesdata";
    public static final String mBroadcastUserDataAction = "ru.petrbardin.touristsbook.actions.userdata";
    public static final String mBroadcastGPSAction = "ru.petrbardin.touristsbook.actions.gps";

    public static final int PERMISSIONS_CODE = 109;

    private IntentFilter mIntentFilter;

    private AccountHeader.Result headerResult = null;
    private Drawer.Result drawerResult = null;

    public Integer LOGIN_TYPE;
    public Activity This;

    Timer timer_updateplaces;
    Timer timer_updateusers;

    API api;
    Preferences pref;

    private double[] userGPS = new double[]{-1, -1};

    public API.User user = null;
    public ArrayList<API.Place> places = new ArrayList<API.Place>();


    MapFragment fMap;
    PlacesListFragment fPlacesList;
    RatingFragment fRating;

    android.support.v4.app.FragmentTransaction fragmentTransaction;
    android.support.v4.app.FragmentManager fragmentManager;

    int activeFragment = 0;
    int lastFragment = 0;

    TextView ndh_tv_email;
    TextView ndh_tv_user;
    TextView ndh_tv_score;
    ImageView ndh_iv_photo;
    ActionBar ab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        This = this;

        pref = new Preferences(this);
        api = new API(this.getApplicationContext());

        LOGIN_TYPE = getIntent().getIntExtra("LOGIN_TYPE", -1);
        if(LOGIN_TYPE != -1) {
            String LOGIN_USER = pref.Username();
            String LOGIN_PASS = pref.Password();
        }
        pref.Email(getIntent().getStringExtra("LOGIN_EMAIL"));

        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory("yandex.intent.category.MAPKIT");


        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(mBroadcastPlacesDataAction);
        mIntentFilter.addAction(mBroadcastUserDataAction);
        mIntentFilter.addAction(mBroadcastGPSAction);

        Intent serviceIntent = new Intent(this, DBContentProviderService.class);
        startService(serviceIntent);


        fMap = new MapFragment();
        fPlacesList = new PlacesListFragment();
        fRating = new RatingFragment();

        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frgmCont, fMap);
        fragmentTransaction.commit();

        Toolbar toolbar = (Toolbar) findViewById(R.id.pia_toolbar__);
        setSupportActionBar(toolbar);
        ab = getSupportActionBar();
        ab.setTitle(R.string.nd_home);

        drawerResult = new Drawer()
                .withActivity(this)
                .withToolbar(toolbar)
                .withActionBarDrawerToggle(true)
                .withHeader(R.layout.drawer_header)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName(R.string.nd_home).withIcon(FontAwesome.Icon.faw_home),
                        new PrimaryDrawerItem().withName(R.string.nd_allplaces).withIcon(FontAwesome.Icon.faw_beer),
                        new PrimaryDrawerItem().withName(R.string.nd_rating).withIcon(FontAwesome.Icon.faw_gamepad),
                        new DividerDrawerItem(),
                        new SecondaryDrawerItem().withName(R.string.nd_logout).withIcon(FontAwesome.Icon.faw_close),
                        new SecondaryDrawerItem().withName(R.string.nd_settings).withIcon(FontAwesome.Icon.faw_gears)
                )
                .withOnDrawerListener(new Drawer.OnDrawerListener() {
                    @Override
                    public void onDrawerOpened(View drawerView) {
                        InputMethodManager inputMethodManager = (InputMethodManager) MainActivity.this.getSystemService(Activity.INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(MainActivity.this.getCurrentFocus().getWindowToken(), 0);
                    }
                    @Override
                    public void onDrawerClosed(View drawerView) {}

                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {}
                })
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id, IDrawerItem drawerItem) {
                        if (drawerItem instanceof Nameable) {
                            Nameable item = (Nameable) drawerItem;
                            int i = item.getNameRes();
                            if(i == R.string.nd_logout){
                                pref.LogOut();

                                Intent n = getBaseContext().getPackageManager()
                                        .getLaunchIntentForPackage(getBaseContext().getPackageName())
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                        .putExtra("ACTION", -1);
                                startActivity(n);

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        fragmentTransaction = fragmentManager.beginTransaction();
                                        try{
                                            fragmentTransaction.detach(fMap);
                                            fragmentTransaction.detach(fPlacesList);
                                            fragmentTransaction.detach(fRating);
                                            fragmentTransaction.remove(fMap);
                                            fragmentTransaction.remove(fPlacesList);
                                            fragmentTransaction.remove(fRating);
                                        } catch (Exception ignored) {}
                                        fragmentTransaction.commit();

                                        System.exit(0);
                                    }
                                });
                            } else if (i == R.string.nd_settings){
                               
                            } else {
                                fragmentTransaction = fragmentManager.beginTransaction();
                                lastFragment = activeFragment;
                                if (i == R.string.nd_home) {
                                    fragmentTransaction.replace(R.id.frgmCont, fMap);
                                    activeFragment = 0;
                                    ab.setTitle(R.string.nd_home);
                                } else if (i == R.string.nd_allplaces) {
                                    fPlacesList.setPlaces(places);
                                    fragmentTransaction.replace(R.id.frgmCont, fPlacesList);
                                    activeFragment = 1;
                                    ab.setTitle(R.string.nd_allplaces);
                                } else if (i == R.string.nd_rating) {
                                    fRating.setUserData(pref.Username(), pref.Password());
                                    fragmentTransaction.replace(R.id.frgmCont, fRating);
                                    activeFragment = 2;
                                    ab.setTitle(R.string.nd_rating);
                                }
                                fragmentTransaction.commit();
                            }
                        }
                    }
                })
                .build();

        initNDH();
    }

    public static void openPlaceInfo(Context ctx, API.Place a){
        Intent i = new Intent(ctx, PlaceInfoActivity.class);
        i.putExtra("data", a);
        ctx.startActivity(i);
    }

    public static void openPlaceInfo(Context ctx, ArrayList<API.Place> a, int uid){
        Intent i = new Intent(ctx, PlaceInfoActivity.class);
        i.putExtra("data", a.get(uid));
        ctx.startActivity(i);
    }

    private void initNDH(){
        View headerView = drawerResult.getHeader();
        ndh_tv_email = (TextView) headerView.findViewById(R.id.ndh_email);
        ndh_tv_user = (TextView) headerView.findViewById(R.id.ndh_username);
        ndh_tv_score = (TextView) headerView.findViewById(R.id.ndh_score);
        ndh_iv_photo = (ImageView) headerView.findViewById(R.id.ndh_person_iv);

        View.OnClickListener ndh_onclick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            openProfile(getBaseContext(), user, places);
            }
        };
        headerView.findViewById(R.id.imageView5).setOnClickListener(ndh_onclick);
        headerView.findViewById(R.id.ndh_person_iv).setOnClickListener(ndh_onclick);
    }

    public static void openProfile(Context ctx, API.User u, ArrayList<API.Place> p){
        Intent intent = new Intent(ctx, ProfileActivity.class);
        intent.putExtra("userdata", u);
        intent.putParcelableArrayListExtra("placesdata", p);
        ctx.startActivity(intent);
    }

    public static void openProfile(final Context ctx, final API.User u){
        final API a = new API(ctx);
        Preferences p = new Preferences(ctx);
        a.getPlaces(a.genCred(p.Username(), p.Password(), new double[]{-1, -1}), new FutureCallback<API.Places>() {
            @Override
            public void onCompleted(Exception e, API.Places res) {
                if(e == null && res != null){
                    MainActivity.openProfile(ctx, u, a.getCapturedPlaces(res.data, u));
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(mReceiver, mIntentFilter);
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(mBroadcastUserDataAction)) {
                user = (API.User)intent.getParcelableExtra("data");
                pref.Email(user.email);
                ndh_tv_email.setText(pref.Email());
                ndh_tv_user.setText(pref.Username());
                ndh_tv_score.setText(String.valueOf(user.score));
                Glide.with(getApplicationContext()).load(user.photo).into(ndh_iv_photo);
            } else if (intent.getAction().equals(mBroadcastPlacesDataAction)) {
                places = intent.getParcelableArrayListExtra("data");
            } else if (intent.getAction().equals(mBroadcastGPSAction)) {
                userGPS = intent.getDoubleArrayExtra("data");
            }
        }
    };

    @Override
    protected void onStop(){
        try {
            unregisterReceiver(mReceiver);
            Intent stopIntent = new Intent(MainActivity.this,
                    DBContentProviderService.class);
            stopService(stopIntent);
        }catch(Exception ignored) {}
        super.onStop();
    }

    @Override
    protected void onPause() {
        unregisterReceiver(mReceiver);
        super.onPause();
    }
    
    @Override
    public void onBackPressed() {
        if (drawerResult.isDrawerOpen()) {
            drawerResult.closeDrawer();
        } else {
            drawerResult.setSelection(lastFragment);
            fragmentTransaction = fragmentManager.beginTransaction();
            int af = activeFragment;
            if (lastFragment == R.string.nd_home) {
                fragmentTransaction.replace(R.id.frgmCont, fMap);
                activeFragment = 0;
                ab.setTitle(R.string.nd_home);
            } else if (lastFragment == R.string.nd_allplaces) {
                fPlacesList.setPlaces(places);
                fragmentTransaction.replace(R.id.frgmCont, fPlacesList);
                activeFragment = 1;
                ab.setTitle(R.string.nd_allplaces);
            } else if (lastFragment == R.string.nd_rating) {
                fRating.setUserData(pref.Username(), pref.Password());
                fragmentTransaction.replace(R.id.frgmCont, fRating);
                activeFragment = 2;
                ab.setTitle(R.string.nd_rating);
            }
            fragmentTransaction.commit();
            lastFragment = af;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void toast(String a){
        Toast.makeText(getApplicationContext(), a, Toast.LENGTH_SHORT).show();
    }


    public static ArrayList<API.Place> places_to_ArrayList(API.Places a){
        ArrayList<API.Place> c = new ArrayList<>();
        for (API.Place b : a.data) {
            c.add(b);
        }
        return c;
    }

    public static API.Place[] places_from_ArrayList(ArrayList<API.Place> a){
        return a.toArray(new API.Place[a.size()]);
    }

}
