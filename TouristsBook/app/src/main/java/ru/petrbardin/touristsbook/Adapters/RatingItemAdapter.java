package ru.petrbardin.touristsbook.Adapters;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.data.StreamAssetPathFetcher;

import java.util.ArrayList;
import java.util.List;

import ru.petrbardin.touristsbook.DataProvider.API;
import ru.petrbardin.touristsbook.R;


public class RatingItemAdapter extends RecyclerView.Adapter<RatingItemAdapter.ItemViewHolder> {
    Context ctx;
    List<API.User> items;
    OnRatingItemClick listener;

    public RatingItemAdapter(List<API.User> items, OnRatingItemClick l, Context ctx) {
        this.items = items;
        this.listener = l;
        this.ctx = ctx;
    }

    @Override
    public RatingItemAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.rf_rv_item, parent, false);
        return new ItemViewHolder(v);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        holder.bind(items.get(position), this.listener, position+1);
        Glide.with(this.ctx).load(items.get(position).photo).into(holder.iv);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView tv;
        TextView tv1;
        TextView tv2;
        ImageView iv;
        CardView cv;

        public ItemViewHolder(View itemView) {
            super(itemView);

            tv = (TextView) itemView.findViewById(R.id.rf_rv_item_tv);
            tv1 = (TextView) itemView.findViewById(R.id.rf_rv_item_tv1);
            tv2 = (TextView) itemView.findViewById(R.id.rf_rv_item_num);
            iv = (ImageView) itemView.findViewById(R.id.rf_rv_item_iv);
            cv = (CardView) itemView.findViewById(R.id.rf_item_cv);
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        void bind(final API.User a, final OnRatingItemClick listener, int i) {
            tv1.setText(a.user);
            tv.setText(String.valueOf(a.score));
            tv2.setText(String.format("#%s", String.valueOf(i)));
            cv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClick(view, a);
                }
            });
        }
    }
}
