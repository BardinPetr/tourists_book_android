package ru.petrbardin.touristsbook.DataProvider;

import android.annotation.TargetApi;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.IBinder;

import com.koushikdutta.async.future.FutureCallback;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;

import ru.petrbardin.touristsbook.MainActivity;
import ru.petrbardin.touristsbook.R;

/*
import android.os.Parcelable;

import com.koushikdutta.async.future.FutureCallback;

import java.io.Serializable;
import java.util.Timer;
import java.util.TimerTask;

import ru.petrbardin.touristsbook.DataProvider.API;

import ru.petrbardin.touristsbook.MainActivity;
import ru.petrbardin.touristsbook.R;
*/
public class DBContentProviderService extends Service {
    private Timer timer_updateplaces;
    private Timer timer_updateusers;
    TimerTask update0;
    TimerTask update1;

    API api;
    Preferences pref;

    private double[] userGPS = new double[]{-1, -1};

    private IntentFilter mIntentFilter;
    private BroadcastReceiver mReceiver2;

    public boolean firstgps = true;

    public DBContentProviderService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();

        timer_updateplaces = new Timer();
        timer_updateusers = new Timer();

        api = new API(this.getApplicationContext());
        pref = new Preferences(this.getApplicationContext());

        update0 = new TimerTask() {
            @Override
            public void run() {
                if(userGPS[0] == -1){
                    return;
                }
                api.getPlaces(api.genCred(pref.Username(), pref.Password(), userGPS), new FutureCallback<API.Places>() {
                    @Override
                    public void onCompleted(Exception e, API.Places res) {
                        if(e == null && res != null) {
                            if (res.status == 200) {
                                ArrayList<API.Place> b = places_to_ArrayList(res);
                                Intent broadcastIntent = new Intent();
                                broadcastIntent.setAction(MainActivity.mBroadcastPlacesDataAction);
                                broadcastIntent.putParcelableArrayListExtra("data", b);
                                sendBroadcast(broadcastIntent);
                            }
                        }
                    }
                });
            }
        };

        update1 = new TimerTask() {
            @Override
            public void run() {
                if(userGPS[0] == -1){
                    return;
                }
                api.getUser(api.genCred(pref.Username(), pref.Password(), userGPS), pref.Username(), new FutureCallback<API.Users>(){
                    @Override
                    public void onCompleted(Exception e, API.Users res) {
                        if(e == null && res != null) {
                            if (res.status == 200) {
                                Intent broadcastIntent = new Intent();
                                broadcastIntent.setAction(MainActivity.mBroadcastUserDataAction);
                                broadcastIntent.putExtra("data", res.data);
                                sendBroadcast(broadcastIntent);
                            }
                        }
                    }
                });
            }
        };

        mReceiver2 = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(MainActivity.mBroadcastGPSAction)) {
                    userGPS = intent.getDoubleArrayExtra("data");

                    if(firstgps){
                        timer_updateplaces.scheduleAtFixedRate(update0, 0, 60000);
                        timer_updateusers.scheduleAtFixedRate(update1, 0, 60000);
                        update0.run();
                        update1.run();
                        
                        firstgps = false;
                    }
                }
            }
        };

        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(MainActivity.mBroadcastGPSAction);
        this.registerReceiver(mReceiver2, mIntentFilter);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_REDELIVER_INTENT;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    public void onTaskRemoved(Intent rootIntent) {
        stop_timers();
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public void onDestroy() {
        stop_timers();
        super.onDestroy();
    }

    public void stop_timers(){
        try{
            this.unregisterReceiver(mReceiver2);
            timer_updateplaces.cancel();
            timer_updateplaces.purge();
            timer_updateusers.cancel();
            timer_updateusers.purge();
        } catch (Exception ignored){ }
    }

    public ArrayList<API.Place> places_to_ArrayList(API.Places a){
        return a.data;
    }

    public API.Place[] places_from_ArrayList(ArrayList<API.Place> a){
        return a.toArray(new API.Place[a.size()]);
    }
}
