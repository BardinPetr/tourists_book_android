package ru.petrbardin.touristsbook.Fragments;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.annotation.UiThread;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.koushikdutta.async.future.FutureCallback;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import ru.petrbardin.touristsbook.DataProvider.API;
import ru.petrbardin.touristsbook.DataProvider.Preferences;
import ru.petrbardin.touristsbook.Dialogs.PlaceDialog;
import ru.petrbardin.touristsbook.Dialogs.ResultDialog;
import ru.petrbardin.touristsbook.MainActivity;
import ru.petrbardin.touristsbook.R;
import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.MapView;
import ru.yandex.yandexmapkit.OverlayManager;
import ru.yandex.yandexmapkit.overlay.OnOverlayItemListener;
import ru.yandex.yandexmapkit.overlay.Overlay;
import ru.yandex.yandexmapkit.overlay.OverlayItem;
import ru.yandex.yandexmapkit.overlay.balloon.BalloonItem;
import ru.yandex.yandexmapkit.overlay.location.MyLocationItem;
import ru.yandex.yandexmapkit.overlay.location.OnMyLocationListener;
import ru.yandex.yandexmapkit.utils.GeoPoint;



public class MapFragment extends Fragment implements OnMyLocationListener, OnOverlayItemListener {
    MapController mapCtrl;
    OverlayManager overlayMgr;
    Overlay overlay;
    MapView mapView;

    double userGPS[] = {-1, -1};

    ArrayList<API.Place> places = null;
    API.User user = null;

    private IntentFilter mIntentFilter;
    private BroadcastReceiver mReceiver2;

    private Timer timer_updateplaces;
    private MapFragment This;

    API api;
    Preferences pref;
    private boolean firstrun = true;

    public MapFragment() {
    }

    public static MapFragment newInstance() {
        return new MapFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        This = this;

        api = new API(this.getContext());
        pref = new Preferences(this.getContext());

        final View v = inflater.inflate(R.layout.fragment_map, container, false);

        mapView = (MapView) v.findViewById(R.id.map2);
        mapCtrl = mapView.getMapController();
        mapCtrl.initializeScreenButtons();
        mapCtrl.showFindMeButton(true);
        mapCtrl.showZoomButtons(true);
        mapCtrl.showJamsButton(false);

        overlayMgr = mapCtrl.getOverlayManager();
        mapCtrl.getOverlayManager().getMyLocation().setEnabled(true);
        mapCtrl.getOverlayManager().getMyLocation().addMyLocationListener(this);

        overlay = new Overlay(mapCtrl);

        checkPermission();

        createTimer();

        ImageButton cb = (ImageButton) v.findViewById(R.id.catch_btn);
        cb.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                api.Catch(api.genCred(pref.Username(), pref.Password(), userGPS), new FutureCallback<API.Capture>() {
                    @Override
                    public void onCompleted(Exception e, API.Capture res) {
                        if(e == null && res != null) {
                            toast(res.status.toString());
                            ResultDialog rd = new ResultDialog(This.getActivity(), res.status);
                            rd.show();

                            if (res.score != 0) {
                                api.getUser(api.genCred(pref.Username(), pref.Password(), userGPS), pref.Username(), new FutureCallback<API.Users>(){
                                    @Override
                                    public void onCompleted(Exception e, API.Users res) {
                                        if(e == null && res != null) {
                                            if (res.status == 200) {
                                                user = res.data;
                                                Intent broadcastIntent = new Intent();
                                                broadcastIntent.setAction(MainActivity.mBroadcastUserDataAction);
                                                broadcastIntent.putExtra("data", res.data);
                                                getActivity().sendBroadcast(broadcastIntent);
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    }
                });
            }
        });

        mReceiver2 = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(MainActivity.mBroadcastPlacesDataAction)) {
                    places = intent.getParcelableArrayListExtra("data");
                } else if (intent.getAction().equals(MainActivity.mBroadcastUserDataAction)) {
                    user = intent.getParcelableExtra("data");
                }
            }
        };

        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(MainActivity.mBroadcastPlacesDataAction);
        mIntentFilter.addAction(MainActivity.mBroadcastUserDataAction);
        this.getActivity().registerReceiver(mReceiver2, mIntentFilter);
        return v;
    }


    @SuppressWarnings( {"MissingPermission"})
    private void createTimer(){
        timer_updateplaces = new Timer();
        TimerTask update = new TimerTask() {
            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void run() {
                        try {
                            if(userGPS[0] == -1 || user == null || places == null)
                                return;

                            overlayMgr.removeOverlay(overlay);
                            overlay = new Overlay(mapCtrl);
                            for (API.Place a : places) {
                                boolean g = !user.captured.contains(a.uid);
                                Drawable d = This.getActivity().getDrawable((g ? R.drawable.ic_place_blue_48dp : R.drawable.ic_place_red_48dp));
                                OverlayItem oI = new OverlayItem(new GeoPoint(a.gps[0], a.gps[1]), d);
                                BalloonItem bOI = new BalloonItem(This.getActivity(), oI.getGeoPoint());
                                bOI.setText(a.name);
                                oI.setBalloonItem(bOI);
                                oI.setOverlayItemListener(MapFragment.this);
                                overlay.addOverlayItem(oI);
                            }
                            overlayMgr.addOverlay(overlay);

                        } catch (Exception ignored){}
                    }
                });
            }
        };
        timer_updateplaces.scheduleAtFixedRate(update, 0, 2000);
    }


    public void toast(String a){
        Toast.makeText(This.getContext(), a, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        try{
            this.getContext().unregisterReceiver(mReceiver2);
        } catch(Exception ignored){}
        super.onDetach();
    }

    @Override
    public void onStop() {
        try{
            timer_updateplaces.cancel();
            this.getContext().unregisterReceiver(mReceiver2);
        } catch(Exception ignored){}
        super.onStop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MainActivity.PERMISSIONS_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mapCtrl.getOverlayManager().getMyLocation().refreshPermission();
                }
            }
        }
    }


    @Override
    public void onMyLocationChange(MyLocationItem gps) {
        userGPS[0] = gps.getGeoPoint().getLat();
        userGPS[1] = gps.getGeoPoint().getLon();

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(MainActivity.mBroadcastGPSAction);
        broadcastIntent.putExtra("data", userGPS);
        this.getActivity().getApplication().sendBroadcast(broadcastIntent);
    }

    private void checkPermission() {
        int permACL = ActivityCompat.checkSelfPermission(this.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION);
        int permAFL = ActivityCompat.checkSelfPermission(this.getContext(), Manifest.permission.ACCESS_FINE_LOCATION);

        if (permACL != PackageManager.PERMISSION_GRANTED ||
                permAFL != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this.getActivity(),
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION}, MainActivity.PERMISSIONS_CODE);
        }
    }

    @Override
    public void onClick(OverlayItem overlayItem) {
        for(API.Place i: places){
            if(i.getName() == overlayItem.getBalloonItem().getText()){
                PlaceDialog pd = new PlaceDialog(This.getActivity(), i);
                pd.show();
            }
        }
    }
}