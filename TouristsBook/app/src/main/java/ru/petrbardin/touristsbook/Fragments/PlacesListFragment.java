package ru.petrbardin.touristsbook.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.koushikdutta.async.future.FutureCallback;

import java.util.ArrayList;
import java.util.Arrays;

import ru.petrbardin.touristsbook.Adapters.OnPlaceItemClick;
import ru.petrbardin.touristsbook.DataProvider.API;
import ru.petrbardin.touristsbook.DataProvider.Preferences;
import ru.petrbardin.touristsbook.MainActivity;
import ru.petrbardin.touristsbook.R;

public class PlacesListFragment extends Fragment {
    ArrayList<API.Place> places = new ArrayList<>();

    View view;
    RecyclerView recyclerView;

    public PlacesListFragment() {
    }

    public static PlacesListFragment newInstance() {
        PlacesListFragment fragment = new PlacesListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_places_list, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager MyLayoutManager = new LinearLayoutManager(getActivity());
        MyLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setAdapter(new PlaceCardAdapter(places, new OnPlaceItemClick() {
            @Override
            public void onClick(View v, int uid) {
                MainActivity.openPlaceInfo(getContext(), places, uid);
            }
        }));
        recyclerView.setLayoutManager(MyLayoutManager);
        return view;
    }

    public void setPlaces(ArrayList<API.Place> places) {
        this.places = places;
    }

    public class PlaceCardAdapter extends RecyclerView.Adapter<PlaceCardViewHolder> {
        private ArrayList<API.Place> list;
        OnPlaceItemClick clickListener;

        public PlaceCardAdapter(ArrayList<API.Place> Data, OnPlaceItemClick cl) {
            list = Data;
            clickListener = cl;
        }

        @Override
        public PlaceCardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recycle_items_place, parent, false);
            return new PlaceCardViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final PlaceCardViewHolder holder, final int position) {
            holder.titleTextView.setText(list.get(position).getName());
            holder.rewardTextView.setText(String.valueOf(list.get(position).getReward()));
            list.get(position).setPhoto(list.get(position).getPhoto());
            Glide.with(getContext())
                    .load(list.get(position).getPhoto())
                    .into(holder.coverImageView);
            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.onClick(v, position);
                }
            });
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }

    public class PlaceCardViewHolder extends RecyclerView.ViewHolder {
        public TextView titleTextView;
        public TextView rewardTextView;
        public ImageView coverImageView;
        public CardView cardView;

        public PlaceCardViewHolder(View v) {
            super(v);
            titleTextView = (TextView) v.findViewById(R.id.titleTextView);
            rewardTextView = (TextView) v.findViewById(R.id.rewardTextView);
            coverImageView = (ImageView) v.findViewById(R.id.coverImageView);
            cardView = (CardView) v.findViewById(R.id.place_item_cv);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
